from bintree import AVL, BinTree

# ---------------------
# Exercices about BinTress

def hauteur(B: BinTree) -> int:
    if B == None:
        return -1
    else:
        return 1 + max(hauteur(B.right), hauteur(B.left))
    

def taille(B: BinTree) -> int:
    if B == None:
        return 0
    else:
        return 1 + taille(B.right) + taille(B.left)

# ---------------------
# Exercices about AVLs


def deseq(B: BinTree) -> int: # desequilibre / 14 main
    return hauteur(B.left) - hauteur(B.right)

def rdg(A: BinTree) -> AVL: # rotation gauche droite / 14 mai
    D = A
    F = D.right
    E = F.left

    D.right = E.left
    F.left = E.right
    E.right = F
    E.left = D

    if E.bal == 1:
        D.bal = 1
        F.bal = 0
    elif E.bal == 0:
        D.bal = 0
        F.bal = 0
    else:
        D.bal = 0
        F.bal = -1
    
    E.bal = 0
    return E