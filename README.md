# S2 - Python

All you need for Python tutorial S2 Undergraduates - Epita

Tutorial subjects, old exams and more info can be found on [Moodle](https://moodle.cri.epita.fr/course/view.php?id=1105)


## algo_py

It currently contains:
- [`matrix.py`](algo_py/matrix.py) contains the basic matrix functions
- [`bintree.py`](algo_py/bintree.py) contains the `BinTree` class
- [`queue.py`](algo_py/queue.py) contains the `Queue` class

## Matrix

The [matrix/files directory](matrix/files) contains matrices as .txt files to help you test your algorithms (in particular for the Harry Potter exercise). To load those matrices the `load` function in `matrix.py` can be used.

## Bintrees

The [`bintrees_examples_all.py`](bintrees/bintrees_examples_all.py) file contains bintrees to help you test your algorithms.

## Exercices

All the functions that we studied in class are in [`exercices.py`](algo_py/exercices.py)
I will copy every functions from my class notebook.