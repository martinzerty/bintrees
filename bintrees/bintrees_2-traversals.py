#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Undergraduate Epita - S2
'''

from algo_py import bintree

#-------------------------------------------------------------------------------
#2.1

'''
DFS: Depth-First Search
'''

def dfs(B):
    if B == None:
        # terminal case
        pass
    else:
        # preorder / prefix
        dfs(B.left)
        # inorder
        dfs(B.right)
        # postorder / suffix
        
        
def __myprint(x):
    '''
    print element x without endline
    '''
    print(x, sep='', end='')
  
  
def dfs_displayAA(B):
    '''
    B: bintree
    display the Algebraic Abstract Type representation of the bintree B
    '''
    if B == None:
        __myprint('_')
    else:
        __myprint('<' + str(B.key) + ',')     
        # + is the concatenation operator on sequences
        dfs_displayAA(B.left)
        __myprint(',')
        dfs_displayAA(B.right)
        __myprint('>')
        
#-------------------------------------------------------------------------------
#2.2

'''
BFS: Breadth-First Search (Level order traversal)
'''

def BFS(B):
    '''
    B: bintree
    prints the keys of the bintree B in hierarchical order
    '''
    if B != None:
        q = queue.Queue()
        q.enqueue(B)
        while not q.isempty():
            B = q.dequeue()
            print(B.key, end= ' ')
            if B.left != None:
                q.enqueue(B.left)
            if B.right != None:
                q.enqueue(B.right)

#-------------------------------------------------------------------------------

