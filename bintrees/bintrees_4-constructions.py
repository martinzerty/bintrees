#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Undergraduate Epita - S2
'''

from algo_py import bintree

#-------------------------------------------------------------------------------
#4.1

def transpose(B):
    if B == None:
        return None
    else:
        return bintree.BinTree(2*B.key, transpose(B.right), transpose(B.left))

def transpose2(B):
    if B == None:
        return None
    else:
        rT = 2 * B.key
        GT = transpose2(B.right)
        DT = transpose2(B.left)
        return bintree.BinTree(rT, GT, DT)
#-------------------------------------------------------------------------------
#4.2

class BinTreeSize:
    def __init__(self, key, left, right, size):
        self.key = key
        self.left = left
        self.right = right
        self.size = size

# __copySize(B) returns the pair (copy of B: BinTreeSize, its size: int)
def __copySize(B):
    '''
    B: bintree
    returns (C, size) where
    C is the conversion of B into a BinTreeSize
    size is the size of C
    '''
    if B == None:
        return(None, 0)
    else:
        (left, size1) = __copySize(B.left)
        (right, size2) = __copySize(B.right)
        size = 1 + size1 + size2
        return (BinTreeSize(B.key, left, right, size), size)

# another version
def __copySize2(B):
    '''
    B: bintree
    returns (C, size) where
    C is the conversion of B into a BinTreeSize
    size is the size of C
    '''
    if B == None:
        return(None, 0)
    else:
        C = BinTreeSize(B.key, None, None, 1)
        (C.left, size1) = __copySize2(B.left)
        (C.right, size2) = __copySize2(B.right)
        C.size += size1 + size2
        return (C, C.size)

def copyWithSize(B):
    '''
    B: bintree
    returns the conversion of B into a BinTreeSize
    '''
    (C, size) = __copySize(B)
    return C

#-------------------------------------------------------------------------------
