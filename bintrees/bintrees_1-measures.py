#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Undergraduate Epita - S2
'''

from algo_py import bintree

#-------------------------------------------------------------------------------
#1.1

def size(B):
    '''
    B: bintree
    return the size of the bintree B
    '''
    if B == None:
        return 0
    else:
        return 1 + size(B.left) + size(B.right)

#-------------------------------------------------------------------------------
#1.2

def height(B):
    '''
    B: bintree
    return the height of the bintree B
    '''
    if B == None:
        return -1
    else:
        return 1 + max(height(B.left), height(B.right))


#-------------------------------------------------------------------------------
