#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Undergraduate Epita - S2
'''

from algo_py import bintree, queue

#-------------------------------------------------------------------------------
#5.1

def aux_kinship(B, x, d):
    if B == None:
        return -1
    else:
        if B.key == x:
            return d
        else:
            res = aux_kinship(B.left, x, d+1)
            if res != -1:
                return res
            else:
                return aux_kinship(B.right, x, d+1)

def get_kinship(B, x, y):
    if B == None:
        return -1
    else:
        if B.key == x:
            return aux_kinship(B, y, 0)
        else:
            if B.key == y:
                return aux_kinship(B, x, 0)
            else:
                res = get_kinship(B.left, x, y)
                if res != -1:
                    return res
                else:
                    return get_kinship(B.right, x, y)

#-------------------------------------------------------------------------------
#5.2

def get_node(B, lvl, n):
    if B == None:
        return None
    else:
        cur = queue.Queue()
        nxt = queue.Queue()
        cur.enqueue(B)
        
        while not cur.isempty() and lvl > 0:
            while not cur.isempty():
                x = cur.dequeue()
                if x.left != None:
                    nxt.enqueue(x.left)
                if x.right != None:
                    nxt.enqueue(x.right)
            lvl = lvl - 1
            cur, nxt = nxt, cur
        
        while not cur.isempty() and n > 0:
            x = cur.dequeue()
            n = n - 1

        if not cur.isempty():
            return cur.dequeue().key
        else:
            return None

#-------------------------------------------------------------------------------
