# -*- coding: utf-8 -*-
"""
S2 Matrices: maths and tests
"""

import matrix

#-------------------------------------------------------------------------------
#1.3 Addition

# first version: the result matrix is built before
def add_matrices(A, B):
    '''
    A, B: non-empty matrices
    returns the sum of matrices A and B
    '''
    (l, c) = (len(A), len(A[0]))
    if len(B) != l or len(B[0]) != c:
        raise Exception("add_matrices: incompatible dimensions")
    M = matrix.init(l, c, 0)
    for i in range(l):
        for j in range(c):
            M[i][j] = A[i][j] + B[i][j]
    return M

# second version: the result matrix is built as we go along
def add_matrices2(A, B):
    '''
    A, B: non-empty matrices
    returns the sum of matrices A and B
    '''
    (l, c) = (len(A), len(A[0]))
    if (l, c) != (len(B), len(B[0])):
        raise Exception("add_matrices2: incompatible dimensions")
    M = []
    for i in range(l):
        L = []
        for j in range(c):
            L.append(A[i][j] + B[i][j])
        M.append(L)
    return M

#-------------------------------------------------------------------------------
#1.4 Product
    
def mult_matrices(A, B):
    '''
    A, B: non-empty matrices
    returns the product of matrices A and B
    '''
    m = len(A)
    n = len(A[0])
    if n != len(B):
        raise Exception("mult_matrices: incompatible dimensions")
    p = len(B[0])
    M = matrix.init(m, p, 0)
    for i in range(m):
        for j in range(p):
            for k in range(n):
                M[i][j] = M[i][j] + A[i][k] * B[k][j]
    return M


#-------------------------------------------------------------------------------
# 2.1 maxGap

def gaplist(L, n):
    '''
    L: non-empty list
    n: size of the list L
    returns the gap of L
    '''
    valMin = L[0]
    valMax = L[0]
    for i in range(1, n):
        valMin = min(valMin, L[i])
        valMax = max(valMax, L[i])
    return valMax - valMin

def maxGap(M):
    '''
    M: non-empty matrix
    returns the gap of M
    '''
    c = len(M[0])
    mgap = gaplist(M[0], c)
    for i in range(1, len(M)):
        mgap = max(mgap, gaplist(M[i], c))
    return mgap

def maxGap2(M):
    mgap = 0
    (l, c) = (len(M), len(M[0]))
    for i in range(l):
        valMin = M[i][0]
        valMax = M[i][0]
        for j in range(1, c):
            valMin = min(valMin, M[i][j])
            valMax = max(valMax, M[i][j])
        mgap = max(mgap, valMax - valMin)
    return mgap

#-------------------------------------------------------------------------------
#2.2 Symmetry

# with a bool variable
def v_symmetric(M):
    (l, c) = (len(M), len(M[0]))
    ldiv2 = l // 2
    i = 0
    test = True
    while i < ldiv2 and test: 
        j = 0
        while j < c and test:
            test = M[i][j] == M[l-i-1][j]
            j += 1
        i += 1
    return test
    
# without the boolean variable
def v_symmetric2(M):
    (l, c) = (len(M), len(M[0]))
    ldiv2 = l // 2
    (i, j) = (0, c) 
    while i < ldiv2 and j == c: 
        j = 0
        while j < c and M[i][j] == M[l-i-1][j]:
            j += 1
        i += 1
    return j == c

#-------------------------------------------------------------------------------
#2.3 Sub_line

def sub_line(M, L):
    """
    is list L included in matrix M? (is a sub list of one of the lines of M)
    both non empty
    """
    
    (lineM, colM) = (len(M), len(M[0])) 
    n = len(L)
    if n > colM:
        return False
    else:
        i = 0
        ok = False
        while i < lineM and not ok:
            j = 0
            while j < colM-n+1 and not ok:
                k = 0
                while k < n and L[k] == M[i][j+k]:
                    k += 1
                ok = (k == n)
                j += 1
            i += 1
        return ok
    
    
def __equalList(LM, L, start):
    """
    test wether L, of length n is equal to LM from positions start to start + n
    """
    (i, n) = (0, len(L))
    while i < n and LM[start+i] == L[i]:
        i += 1
    return i == n

def sub_line2(M, L):
    (lb, cb, n) = (len(M), len(M[0]), len(L))
    if n > cb:
        return False
    else:
        i = 0
        j = (cb - n) + 1
        while i < lb and j > (cb-n) :
            j = 0 
            while j <= (cb-n) and not __equalList(M[i], L, j):
                j += 1
            if j > (cb-n) :
                i += 1
        return i < lb
