# -*- coding: utf-8 -*-
'''
Magic Squares
@author: Nathalie
'''

from algo_py import matrix

#-------------------------------------------------------------------------------

def SumLine(L):
    s = 0
    for e in L:
        s += e
    return s

def SumColonne(A,j):
    s = 0
    for i in range(len(A)):
        s += A[i][j]
    return s

def Sumdiag(A):
    s1 = 0
    s2 = 0
    n = len(A)
    for i in range(n):
        s1 += A[i][i]
        s2 += A[i][n-i-1]
    return (s1, s2)


def magic_square(A):
    n = len(A)
    if n != len(A[0]):
        raise Exception("not a square matrix")
    (refsum, s) = Sumdiag(A)
    if s != refsum:
        return False
    else:
        i = 0
        while i < n and refsum == SumLine(A[i]) \
                    and refsum == SumColonne(A, i):
            i += 1
        return i == n

#-------------------------------------------------------------------------------

def testNormal(S):
    n = len(S)    
    n2 = n * n
    L = []
    for i in range(n2):
        L.append(False)
    ok = True
    i = 0
    while (i < n) and ok:
        j = 0
        while (j < n) and ok:
            if (S[i][j] <= 0) or (S[i][j] > n2) \
                or L[S[i][j]-1]:
                ok = False
            else:
                L[S[i][j]-1] = True
            j = j + 1
        i = i + 1
    return ok
        
#-------------------------------------------------------------------------------

# movess : [+1, +1], [0, -1]
# assume      n is odd > 2


# first version : test if cell is empty -> if not move [0, -1]
def Siamese_(n):
    '''
    n: odd size of the siamese square
    builds and returns a magic square of n-odd size
    starts in the middle of last line, then going SE
    '''
    S = matrix.init(n, n, 0)
    i = n-1
    j = n // 2
    S[i][j] = 1
    for k in range(2, n*n + 1):
        i2 = (i + 1) % n
        j2 = (j + 1) % n
        if S[i2][j2] == 0:
            (i, j) = (i2, j2)
        else:
            i = i-1
            if i == -1:
                i = n-1
        S[i][j] = k
    return S

# second version: change direction each n*k +1
def Siamese(n):
    '''
    n: odd size of the siamese square
    builds and returns a siamese square of odd size n
    '''
    S = matrix.init(n, n, 0)
    (i, j) = (n - 1, n // 2)
    for val in range(1, n*n + 1):
        S[i][j] = val
        if val % n == 0:
            i = i - 1
            if i == -1:
                i = n-1
        else:
            (i, j) = ((i + 1) % n, (j + 1) % n)
    return S


# second version
def Siamese2(n):
    '''
    n: odd size of the siamese square
    builds and returns a siamese square of odd size n
    '''
    M = matrix.init(n, n, 0)
    (i, j) = (n - 1, n // 2)
    M[i][j] = 1
    for val in range(2, n*n+1):
        if (val-1) % n == 0:
            if i == 0:
                i = n - 1
            else:
                i = i - 1
        else:
            (i, j) = ((i + 1) % n, (j + 1) % n)
        M[i][j] = val
    return M

#-------------------------------------------------------------------------------
