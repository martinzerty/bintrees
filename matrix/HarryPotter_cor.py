# -*- coding: utf-8 -*-
'''
Philosophers Stone
S2 - Matrix tutorial
'''

from algo_py import matrix
from algo_py import timing

# uncomment the lines @timing.timing if you want to see the durations!
# you can use the function max that returns the maximum off all its parameters (here, with 2 or 3 values)


##### Helpers
def posmax(L):
    '''
    position of (one of) the maximum value in list L, non empty
    '''
    pos = 0
    for i in range(1, len(L)):
        if L[i] > L[pos]:
            pos = i
    return pos

def reverse(L):
    '''
    reverse L in place
    '''
    n = len(L)
    for i in range(n//2):
        (L[i], L[n-i-1]) = (L[n-i-1], L[i])
        
#----------------- Greedy Algorithm (algorithme glouton) -----------------------

# matrices are assumed not empty in all the following functions

#@timing.timing
def HarryPotter_greedy(T):
    col = len(T[0])
    j = posmax(T[0])
    s = T[0][j]
    for i in range(1, len(T)):
        jmax = j
        if j > 0 and T[i][j-1] > T[i][jmax]:
            jmax = j-1
        if j < col-1 and T[i][j+1] > T[i][jmax]:
            jmax = j+1
        j = jmax
        s += T[i][j]
    return s

# with the path
def HarryPotter_greedy_path(T):
    col = len(T[0])
    j = posmax(T[0])
    s = T[0][j]
    path = [j]
    for i in range(1, len(T)):
        jmax = j
        if j > 0 and T[i][j-1] > T[i][jmax]:
            jmax = j-1
        if j < col-1 and T[i][j+1] > T[i][jmax]:
            jmax = j+1
        j = jmax
        path.append(j)
        s += T[i][j]
    return (s, path)

#----------------- Dynamic Programming -----------------------------------------


def build_max_matrix_down(T):
    l = len(T)
    c = len(T[0])
    M = matrix.init(l, c, 0)
    
    # copy the first line
    for j in range(c):
        M[0][j] = T[0][j]

    for i in range(1, l):
        M[i][0] = T[i][0] + max(M[i-1][0], M[i-1][1])
        for j in range(1, c-1):
            M[i][j] = T[i][j] + max(M[i-1][j-1], M[i-1][j], M[i-1][j+1])
        M[i][c-1] = T[i][c-1] + max(M[i-1][c-2], M[i-1][c-1])
    
    return M

# going up
def build_max_matrix(T):
    l = len(T)
    c = len(T[0])
    M = matrix.init(l, c, 0)
    
    # copy the last line
    for j in range(c):
        M[l-1][j] = T[l-1][j]

    for i in range(l-2, -1, -1):
        M[i][0] = T[i][0] + max(M[i-1][0], M[i-1][1])
        for j in range(1, c-1):
            M[i][j] = T[i][j] + max(M[i+1][j-1], M[i+1][j], M[i+1][j+1])
        M[i][c-1] = T[i][c-1] + max(M[i+1][c-2], M[i+1][c-1])
    
    return M
    
#@timing.timing
def HarryPotter(T):
    M = build_max_matrix_down(T)
    n = len(M)  # line nb
    return M[n-1][posmax(M[n-1])]

# with the path    
def HarryPotter_path_(T):
    M = build_max_matrix_down(T)
    (l, c) = (len(M), len(M[0]))  
    
    # build the path: going-up in M
    j = posmax(M[l-1])
    val = M[l-1][j]
    path = [j]
    for i in range(l-2, -1, -1):
        jmax = j
        if j > 0 and M[i][j-1] > M[i][jmax]:
            jmax = j-1
        if j < c-1 and M[i][j+1] > M[i][jmax]:
            jmax = j+1
        j = jmax
        path.append(j)
    reverse(path)
    return (val, path)

# with the path    
def HarryPotter_path(T):
    M = build_max_matrix(T)
    val = M[0][posmax(M[0])]
    (_, path) = HarryPotter_greedy_path(M)
    return (val, path)    

#-------------------------------------------------------------------------------
# Brut force... warning: can be long when l, c >= 15, 15

# without the path
def brut(T, i, j, lines, cols):
    '''
    return the value of the best path from (i, j) (going down) in T : lines x cols
    '''
    if i == lines - 1:
        return T[i][j]
    else:
        m = brut(T, i+1, j, lines, cols)
        if j > 0:
            m = max(m, brut(T, i+1, j-1, lines, cols))
        if j < cols - 1:
            m = max(m,  brut(T, i+1, j+1, lines, cols))
        return (m + T[i][j])

#@timing.timing
def HarryPotter_brutforce(T):
    '''
    T not empty, at least 2 x 2 cells
    '''
    (lines, cols) = (len(T), len(T[0]))
    maxi = 0
    for j in range(cols):
        maxi = max(maxi, brut(T, 0, j, lines, cols))
    return maxi


#      BONUS: brut force with the path           

# the list is returned (use +... really not optimized)
def brut_path(T, i, j, lines, cols):
    if i == lines - 1:
        return (T[i][j], [j])
    else:
        (m, L) = brut_path(T, i+1, j, lines, cols)
        if j > 0:
            (mleft, Lleft) = brut_path(T, i+1, j-1, lines, cols)
            if mleft > m:
                m = mleft
                L = Lleft
        if j < cols - 1:
            (mright, Lright) = brut_path(T, i+1, j+1, lines, cols)
            if mright > m:
                m = mright
                L = Lright
        return (m + T[i][j], [j] + L)


def HarryPotter_brutforce_path(T):
    
    maxi = 0
    (lines, cols) = (len(T), len(T[0]))
    for j in range(cols):
        (m, L) = brut_path(T, 0, j, lines, cols)
        if m > maxi:
            (maxi, Lmax) = (m, L)
    return (maxi, Lmax)

'''
"optimization": put path in paremeter and fill it going up (with append), 
then reverse it in call function
'''

#TODO
